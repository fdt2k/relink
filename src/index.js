#!/usr/bin/env node
import path,{dirname} from 'path'
import {homedir} from 'os';

import fs from 'fs'
import program from 'commander';
import { spawnSync } from 'child_process'
import { exit } from 'process';

const root_path = process.cwd();
const nm_path = 'node_modules'


const expanduser = text => text.replace(
  /^~([a-z]+|\/)/,
  (_, $1) => $1 === '/' ? homedir()+'/' : `${dirname(homedir())}/${$1}`
);

const yarn_links_path = expanduser('~/.config/yarn/link/');


const max_depth = 10;

program
    .option('-w, --watch', 'generate watchlist')
    .option('-l, --link', 'I\'m a link')
    .option('-f, --file <file>', 'specify file to use')
    .option('-d, --deps', 'generate links for all deps in package.json')
    .option('-k, --keys <key>', 'generate links for all [keys] in package.json')
    .option('-m, --module', 'use selection as subfolder instead of package')
    .option('-v, --view', 'List existing links')
    .parse(process.argv);



const package_file = program.file || 'package.json';

if( ! fs.existsSync(path.join(root_path,package_file)))
    throw new Error(`file ${package_file} not found`)

const relative_path = path => {
    return path.replace(root_path, '');
}

const readPKG = (packagePath, callback) => {

    const pkg = path.join(packagePath, 'package.json');
    fs.readFile(pkg, 'utf-8', function (error, content) {
        if (error || !content) {
            console.error('Unable to read ' + pkg + ':', error || 'no content');
            return;
        }
        try {
            const config = JSON.parse(content);
            if (config.isParseConfigFailed) {
                console.error('Unable to parse ' + pkg + ':', config.error);
                return;
            }
            callback(config);
        } catch (e) {
            console.error('pkg parse error', e)
        }
    });
}





export const relink = (depth,root) => (config) => {
    console.log(`[RELINK] processing links in "${relative_path(root)}"`)
    if (depth > max_depth) {
        console.error('max_depth reached aborting');
        return ;
    }

    const packages = config.links;
    if (!config.links) {
        console.error('no links defined');
        return;
    }

    const options = config.relink;


    

    let package_path = path.join(root, nm_path);
    


  /*  if ( typeof options !== "undefined" && typeof options.path !== "undefined" ){
        package_path = path.join(package_path,options.path)
    }
*/


    packages.map(pkg => {
        link_pkg(root, pkg);
        let new_path =  path.join(package_path, pkg)
        readPKG(new_path, relink(depth + 1,new_path));
    })
}

const spawn_log = log => {
    log.stderr.length > 0 && console.error(log.stderr.toString())
    log.stdout.length > 0 && console.log(log.stdout.toString())
}


const link_pkg = (cwd, link_pkg) => {
    console.info(`linking ${link_pkg} in ${relative_path(cwd)} `)
    let log = spawnSync(`yarn`, ['link', link_pkg], { cwd })
    spawn_log(log)
}


const watch = config => {
    const packages = config.links;
    if (!config.links) {
        console.log('');
    } else {
        console.log(config.links.map(pk => {
            const dest_folder=  path.join (root_path,nm_path,pk,'package.json');
            const pkg = JSON.parse(fs.readFileSync(dest_folder).toString('utf-8'));
            if(pkg.relink && pkg.relink.watchFolder) {
                return `--watch ${nm_path}/${pk}/${pkg.relink.watchFolder}`
            }
            return `--watch ${nm_path}/${pk}`
        }).join(' '))
    }
}


const create_link_pkg = pkg => {
    let log = spawnSync(`yarn`, ['link'], { cwd: path.join(root_path, 'node_modules', pkg) })
    spawn_log(log)
}

const link_dependencies = config => {
    Object.keys(config.dependencies).map(create_link_pkg)
}


const create_link =  pkg => {
    console.log(pkg)
    let log = spawnSync(`yarn`, ['link'], { cwd: path.join(root_path,   pkg) })
    spawn_log(log)
}


const link = package_json=> {


    let package_path = root_path
    
    const options = package_json.relink
    
    if ( typeof options !== "undefined" && typeof options.path !== "undefined" ){
        package_path = path.join(package_path,options.path)
    }

    spawnSync(`yarn`, ['unlink'], {cwd:root_path})

    let log = spawnSync(`yarn`, ['unlink'], { cwd: package_path})
    spawn_log(log)

    let log2= spawnSync(`yarn`, ['link'], { cwd: package_path})
    spawn_log(log2)
}

const link_keys = key => config => {
    if(!config[key]){
        throw new Error (`Key ${key} not found in package.json`)
    }
    config[key].map(create_link)
}

const  is_dir = _path => fs.lstatSync(_path).isDirectory() 
const  is_symbolic = _path => fs.lstatSync(_path).isSymbolicLink() 

const read_dir = _path => {

    if(is_dir(_path) && !is_symbolic(_path)){
       const list = fs.readdirSync(_path)
        
       for (let item of list ){
           read_dir(path.join(_path,item))
       }
       return list

    }else{
        console.log(_path.replace(yarn_links_path,''),'\t', fs.realpathSync(fs.readlinkSync(_path)))
    }

}

/*
if(program.link){
    spawn_log(spawnSync('yarn',['link']));
}*/

//console.log('yarn relink -- startup');
if(process.env.NODE_ENV === 'production' || process.env.YARN_RELINK==='ignore'){
    console.warn('ignoring  relink command due to env var');
    exit(0);
}

if (program.watch === true) {
    readPKG(root_path, watch)
} else if(program.link === true){
    readPKG(root_path,link)
}else if (program.deps === true) {
    readPKG(root_path, link_dependencies)
} else if (program.keys !== "" && typeof program.keys !== "undefined") {
    readPKG(root_path, link_keys(program.keys))
} else if (program.view ){
    read_dir(yarn_links_path)
} else {
    readPKG(root_path, relink(0,root_path))
}



